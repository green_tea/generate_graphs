//
// Created by ice-tea on 25.08.18.
//

#include "pair_hash.h"

size_t pair_hash::operator()(const std::pair<int, int>& p) const {
    return pair_hash::hasher(p.first) * pair_hash::hasher(p.second);
}
