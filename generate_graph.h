//
// Created by ice-tea on 23.08.18.
//

#include <vector>
#include <iostream>
#include <random>
#include <unordered_set>
#include <functional>
#include <algorithm>
#include <ctime>
#include <utility>
#include "pair_hash.h"

using std::vector;
using std::pair;
typedef vector<vector <int> > graph_t;


class Graph_generator {
private:
    std::default_random_engine gen;

    graph_t union_graphs(const graph_t& first_graph, const graph_t& second_graph, int delta);

public:
    graph_t generate_graph(size_t vertex_count, size_t edges_count, bool is_directed);

    graph_t generate_dublicated_edges_graph(size_t vertex_count, size_t edges_count, bool is_dericted);

    graph_t generate_tree(size_t vertex_count);

    graph_t generate_forest(size_t vertex_count, size_t connectivity_component_count);

    graph_t generate_one_component_graph(size_t vertex_count, size_t edges_count);

    Graph_generator();

    void print_graph(const graph_t& graph, std::ostream& out) const;
};
