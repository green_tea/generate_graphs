//
// Created by ice-tea on 25.08.18.
//

#include <iostream>
#include <utility>

struct pair_hash {
private:
    std::hash<int> hasher;

public:
    size_t operator()(const std::pair<int, int>& p) const;
};