//
// Created by ice-tea on 24.08.18.
//


#include "generate_graph.h"

Graph_generator::Graph_generator() {
    gen = std::default_random_engine(time(0));
}

graph_t Graph_generator::generate_graph(size_t vertex_count, size_t edges_count, bool is_directed) {
    if ((!is_directed) && (vertex_count * (vertex_count - 1) / 2 < edges_count)) throw std::invalid_argument("edges count should be less or eque than vertex count * (vertex count - 1) / 2");
    if ((is_directed) && (vertex_count * (vertex_count - 1) < edges_count)) throw std::invalid_argument("in directed graph edges count should be less or eque than vertex count * (vertex count - 1)");
    graph_t graph(vertex_count);
    std::uniform_int_distribution<int> dist(0, vertex_count - 1);
    std::unordered_set<pair<int, int>, pair_hash> used;
    for (int i = 0; i < edges_count; ++i) {
        int u = dist(gen), v = dist(gen);
        while (used.count(std::make_pair(u, v))) {
            u = dist(gen);
            v = dist(gen);
        }
        used.insert(std::make_pair(u, v));
        if (!is_directed) used.insert(std::make_pair(v, u));
        graph[u].push_back(v);
        if (!is_directed) graph[v].push_back(u);

    }
    return graph;
}

graph_t Graph_generator::generate_dublicated_edges_graph(size_t vertex_count, size_t edges_count,
                                                                        bool is_dericted) {
    graph_t graph(vertex_count);
    if (vertex_count == 0) return graph;

    std::uniform_int_distribution<int> dist(0, vertex_count - 1);

    for (int i = 0; i < edges_count; ++i) {
        int u = dist(gen), v = dist(gen);
        graph[u].push_back(v);
        if (!is_dericted) graph[v].push_back(u);
    }
    return graph;
}


graph_t Graph_generator::generate_tree(size_t vertex_count) {
    graph_t graph(vertex_count);
    if (vertex_count <= 1)
        return graph;
    vector <size_t> permutation(vertex_count);
    std::uniform_int_distribution<int> dist(0, vertex_count - 1);
    for (size_t i = 0; i < permutation.size(); ++i) {
        permutation[i] = dist(gen);
    }
    vector <int> counts(vertex_count);
    for (int i = 0; i < vertex_count - 2; ++i) {
        ++counts[permutation[i]];
    }
    int index = 0, special_index = -1;
    for (int i = 0; i < vertex_count - 2; ++i) {
        while (counts[index++] != 0);
        --index;
        if (special_index >= index) special_index = -1;
        if (special_index != -1) {
            graph[special_index].push_back((int)permutation[i]);
            graph[permutation[i]].push_back(special_index);
            --counts[special_index];
            special_index = (--counts[permutation[i]] == 0) ? (int)permutation[i] : -1;
        }
        else {
            graph[permutation[i]].push_back(index);
            graph[index].push_back((int)permutation[i]);
            --counts[index];
            special_index = (--counts[permutation[i]] == 0) ? (int)permutation[i] : -1;
            ++index;
        }
    }
    int first = -1, second = 0;
    for (int i = 0; i < counts.size(); ++i) {
        if (counts[i] == 0) {
            if (first == -1) first = i;
            else second = i;
        }
    }
    graph[first].push_back(second);
    graph[second].push_back(first);
    return graph;
}

graph_t Graph_generator::union_graphs(const graph_t& first_graph, const graph_t& second_graph, int delta) {
    graph_t graph(first_graph.size());
    for (int from = 0; from < first_graph.size(); ++from) {
        for (auto to : first_graph[from]) {
            graph[from].push_back(to);
        }
    }
    for (int from = 0; from < second_graph.size(); ++from) {
        for (auto to : second_graph[from]) {
            graph[from + delta].push_back(to + delta);
        }
    }
    return graph;
}

graph_t Graph_generator::generate_forest(size_t vertex_count, size_t connectivity_component_count) {
    if (connectivity_component_count > vertex_count) throw std::invalid_argument("connectivity components count should be less or eque than vertex count");
    graph_t graph(vertex_count);
    if (vertex_count == 0) {
        return graph;
    }
    vector <int> sizes(connectivity_component_count);
    std::fill(sizes.begin(), sizes.end(), 1);
    std::uniform_int_distribution<int> dist(0, connectivity_component_count - 1);
    for (int i = 0; i < vertex_count - connectivity_component_count; ++i) {
        size_t index = dist(gen);
        ++sizes[index];
    }
    int sum = 0;
    for (int i = 0; i < connectivity_component_count; ++i) {
        graph_t cur_graph = generate_tree(sizes[i]);
        graph = union_graphs(graph, cur_graph, sum);
        sum += sizes[i];
    }
    return graph;
}


graph_t Graph_generator::generate_one_component_graph(size_t vertex_count, size_t edges_count) {
    if (edges_count + 1 < vertex_count) throw  std::invalid_argument("in one component graph edges count plus one should be more than vertex count");
    graph_t answer = generate_tree(vertex_count);
    std::uniform_int_distribution<int> dist(0, vertex_count - 1);
    for (int i = 0; i < edges_count - vertex_count + 1; ++i) {
        size_t u = dist(gen), v = dist(gen);
        answer[u].push_back(v);
        answer[v].push_back(u);
    }
    return answer;
}

void Graph_generator::print_graph(const graph_t& graph, std::ostream& out) const {
    for (int i = 0; i < graph.size(); ++i) {
        for (auto to : graph[i]) {
            out << i + 1 <<  " " << to + 1 << "\n";
        }
    }
}