//
// Created by ice-tea on 23.08.18.
//

#include <iostream>
#include "generate_graph.h"
using std::cin;
using std::cout;

int main() {
    Graph_generator graph_generator;
    size_t n = 10, m = 15;
    vector <vector <int> > graph = graph_generator.generate_graph(n, m, false);
    graph_generator.print_graph(graph, std::cout);
    cout << "***\n\n\n***\n";
    graph = graph_generator.generate_graph(n, m, true);
    graph_generator.print_graph(graph, std::cout);
    cout << "***\n\n\n***\n";
    graph = graph_generator.generate_dublicated_edges_graph(n, m, false);
    graph_generator.print_graph(graph, std::cout);
    cout << "***\n\n\n***\n";
    graph = graph_generator.generate_dublicated_edges_graph(n, m, true);
    graph_generator.print_graph(graph, std::cout);
    cout << "***\n\n\n***\n";
    graph = graph_generator.generate_tree(n);
    graph_generator.print_graph(graph, std::cout);
    cout << "***\n\n\n***\n";
    graph = graph_generator.generate_forest(n, 3);
    graph_generator.print_graph(graph, std::cout);
    cout << "***\n\n\n***\n";
    graph = graph_generator.generate_one_component_graph(n, m - 6);
    graph_generator.print_graph(graph, std::cout);
}
